<?php namespace Quickjob\Database;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class RelationsTest extends \QuickjobAppTestCase
{
	public function setUp()
	{
		parent::setUp();
		$this->call('GET','/');
		// Waiting for the laravel/framework #4425 issue to know if it will be possible to use absolute paths at the option --path
		// Artisan::call('migrate',array('--path'=>__DIR__ . '/migrations','--env'=>'testing'));
		Artisan::call('migrate',array('--path'=>'_quickjob/database/tests/migrations','--env'=>'testing'));
		Artisan::call('db:seed',array('--class'=>'Quickjob\\Database\\SeedDatabases'));
		$this->dog = \Dog::find(1);
	}
	public function testIfWasMigrated()
	{
		$results = DB::select('select * from owners');
		$this->assertNotNull($results, 'Migrated');
	}
	public function testIfCanGetRelationship()
	{
		$owner = $this->dog->owner;
		$this->assertNotEmpty($owner->name);
	}
}
