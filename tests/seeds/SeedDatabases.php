<?php namespace Quickjob\Database;
use Illuminate\Database\Seeder;
class SeedDatabases extends Seeder {

	public function run()
	{
		$faker = \Faker\Factory::create();

		for ($i = 1; $i < 10; $i++)
		{
			$owner = \Owner::create(array(
				'name' => $faker->userName,
			));
		}

		for ($i = 1; $i < 10; $i++)
		{
			$dog = \Dog::create(array(
				'name' => $faker->userName,
				'owner_id' => $i,
			));
		}
	}

}
