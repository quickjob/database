<?php 

use Quickjob\Database\Model;

function migrate() {
	$migration = new CreateOwnersTable;
	$migration->up();
}

class Dog extends Model {
	protected $table = 'dogs';
	protected $fillable = ['name','owner_id'];
	public static $relationships = array(
		'owner' => array('belongsTo','Owner')
	);
}

class Owner extends Model {
	protected $table = 'owners';
	protected $fillable = ['name'];
	public static $relationships = array(
		'dogs' => array('hasMany','Dog')
	);	
}
