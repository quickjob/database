<?php

use Quickjob\Database\Model;
use Mockery as m;

class ModelTest extends \QuickjobAppTestcase {

	public function tearDown()
	{
		m::close();
	}
	public function testIfModelCanBeCreated() {
		$model = new Model([]);
		$this->assertNotNull($model, 'Model exists.');
	}

	public function getValidator() {
		$connection = m::mock('Illuminate\Validation\PresenceVerifierInterface');
		$connection->shouldReceive('setConnection')->once()->andReturn(true);

		$validation = m::mock('Illuminate\Validation\Validator');
		// var_dump($validation);die;
		$validation->shouldReceive('getPresenceVerifier')
		->once()
		->andReturn($connection);

		$validation->shouldReceive('setAttributeNames')->once()->andReturn($validation);

		return $validation;        
	}

	public function testValidateSuccess() {
		$model = new Model([]);
		$validation = $this->getValidator();
		$validation->shouldReceive('passes')->once()->andReturn(true);
		$model->setValidator($validation);

		$result = $model->validate();

		$this->assertTrue($result);
		$this->assertNull($model->errors());
	}

	public function testValidateFail() {
		$model = new Model([]);
		$validation = $this->getValidator();
		$validation->shouldReceive('passes')->once()->andReturn(false);
		$messages = m::mock('Illuminate\Support\MessageBag');
		$messages->shouldReceive('all')->andReturn(['message']);
		$validation->shouldReceive('messages')->once()->andReturn($messages);
		$model->setValidator($validation);

		$result = $model->validate();

		$this->assertFalse($result);
		$this->assertEquals(['message'],$model->errors());
	}

	public function testAutoPurge() {
		$model = new Model;
		$model->name = 'John Doe';
		$model->password = 'password';
		$model->password_confirmation = 'password';
		$model->_has_id = true;

		$attr   = $model->toArray();
		$model->purgeUnneeded();
		$purged = $model->toArray();
		
		$this->assertNotEquals($attr, $purged);
		$this->assertFalse(isset($purged['password_confirmation']));
		$this->assertFalse(isset($purged['_has_id']));
	}
}
