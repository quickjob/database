<?php namespace Quickjob\Database;

// use Auth;
use Schema;

class PermissionsObserver {
	// public function __construct() {
	// 	$this->user = Auth::user();
	// }
	public function creating($model)
	{
		$model = $this->createdBy($model);
		return $model;
	}

	public function createdBy($model) {
		if(Schema::hasColumn($this->table,'created_by')) {
			$model->created_by = $this->user->id;
		}
		if(Schema::hasColumn($this->table,'criado_por')) {
			$model->criado_por = $this->user->id;
		}
		return $model;
	}
}
