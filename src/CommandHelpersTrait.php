<?php namespace Quickjob\Database;

trait CommandHelpersTrait {
	public function queryFilter($payload) {
		if(isset($payload['page'])) {
			\DB::getPaginator()->setCurrentPage($payload['page']);
		}
		return $this;
	}

	public function getPaginated($query) {
		return $query->paginate(8);
	}
}
