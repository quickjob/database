<?php namespace Quickjob\Database;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;

class DatabaseServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	public function boot() {
		$this->package('quickjob/database');
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app['quickjob.database'] = $this->app->share(function($app)
		{
			// return new Environment($app);
		});

		$this->app->booting(function()
		{
			// $loader = AliasLoader::getInstance();
			// $loader->alias('Database', 'Quickjob\Database\Facades\Database');
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('quickjob.database');
	}

}
