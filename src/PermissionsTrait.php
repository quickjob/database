<?php namespace Quickjob\Database;
use Auth;
use Schema;
trait PermissionsTrait
{
	public static function boot()
	{
		parent::boot();
		$user = \Auth::user();

		static::creating(function($model) use($user) {
			$model->setCreatedBy($user->id);
			$model->setClientId($user->client->id);
			return $model;
		});

		static::deleting(function($model) use($user) {
			$model->checkIfCanDelete($user);
			return $model;
		});

		static::updating(function($model) use($user) {
			$model->checkIfCanUpdate($user);
			return $model;
		});
	}

	public function setCreatedBy($id)
	{
		if(Schema::hasColumn($this->getTable(),'created_by')) {
			$this->created_by = $id;
		}
		if(Schema::hasColumn($this->getTable(),'criado_por')) {
			$this->criado_por = $id;
		}
		return $this;
	}

	public function setClientId($id)
	{
		if(Schema::hasColumn($this->getTable(),'client_id')) {
			$this->client_id = $id;
		}
		if(Schema::hasColumn($this->getTable(),'id_inquilino')) {
			$this->id_inquilino = $id;
		}
		return $this;
	}

	public function getClientIdColumn()
	{
		$column = false;
		if(Schema::hasColumn($this->getTable(),'client_id')) {
			$column = 'client_id';
		}
		if(Schema::hasColumn($this->getTable(),'id_inquilino')) {
			$column = 'id_inquilino';
		}
		return $this->$column;
	}

	// TODO: Improve verifications
	public function checkIfCanDelete($user) {
		if($this->getClientIdColumn() != $user->client->id) {
			throw new UserPermissionException(t('permissions.cannot-delete'), 1);
		}
		return $this;
	}

	// TODO: Improve verifications
	public function checkIfCanUpdate($user) {
		if($this->getClientIdColumn() != $user->client->id) {
			throw new UserPermissionException(t('permissions.cannot-update'), 1);
		}
		return $this;
	}

}
